class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.decimal :price
      t.decimal :discount_price
      t.boolean :enabled
      t.string :image_url
      t.string :category
      t.binary :images
      t.string :permalink
      t.text :description

      t.timestamps null: false
    end
  end
end
