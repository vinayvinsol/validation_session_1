class MySpecialError < StandardError
  def messsge
    "bbobobobo"
  end
end
class Product < ActiveRecord::Base

  attr_accessor :select_box

# validates

  # validates :title, presence: true, on: [:create]
  # validates :title, uniqueness: true, unless: Proc.new { |a| title == "hi there" }
  # validates :title, presence: true, unless: Proc.new { title.blank? }
  # validates :title, confirmation: true, if: Proc.new { title.blank? }
  # validates :title, presence: true, allow_blank: true
  # validates :title, presence: true, allow_nil: true
  # validates :title, presence: true, strict: MySpecialError

# => 1. validates_absence_of

  # validates :enabled, absence: { message: "you should not enter blank value of %{attribute}" }
  # validates :select_box, absence: true
  # validates_absence_of :title, message: "must be boobobo"


# => 2. validates_acceptance_of

  #default datatype of accept option is string any other option will not work in anyway
  #acceptance has a problem working with nil
  #if you give nil as value of attributes it is accepted for every condition

  # validates_acceptance_of :title, accept: "hi there", message: "must be 'hi there'"
  # validates :title, acceptance: true, allow_nil: false
  # validates :title, acceptance: { accept: "hi there", message: "must be 'hi there'" }
  # validates :enabled, acceptance: true      #it will not work
  # validates :enabled, acceptance: { accept: "0" }    #it will not work either
  # validates :enabled, acceptance: { accept: false }    #it will work only
  # validates :ena, acceptance: { accept: "yes" }

# => 3. validates_confirmation_of

  #It also doesnt work if title is nil or true but it certainly works if title is false!

  # validates_confirmation_of :title
  # validates :title, confirmation: { message: "gvjdndnkf" }

# => 4. validates_exclusion_of

  #in or within option is mandatory for validates_exclusion_of otherwise it shows error
  # validates_exclusion_of :title, within: ->(person){ print person }
  #the argument person passed in is the object itself its testcase is as follows
  # validates_exclusion_of :password, within: ->(person) { [person.username, paerson.first_name, person.phone_no] }

  # validates :title, exclusion: %w( mov avi )

  # validates :select_box, exclusion: { in: 0..2 }
  #exclusion will not care if the conditional attribute is not in the database
  #object is always created

  # validates :title, exclusion: { within: %w(hey ho), message: "ishgoifbyi" }
  # validates :title, exclusion: { within: nil, message: "ishgoifbyi" }
  #can not give nil it will throw an error

# => 5. validates_format_of

  # validates_format_of :description, with: /.[jpg|gif]/, without: /.gif/
  #both with or without can not be supplied but one is must

  # validates_format_of :description, with: /\.(jpg|gif)/
  # validates_format_of :description, without: /\.gif/

  # validates_format_of :description, with: /^(jpg)+$/
  # validates_format_of :description, with: /\A(jpg)+\z/
  # validates_format_of :description, with: /^(jpg)+$/, multiline: true
  #safely use \A \z option while using multiline options ^ $ multiline => true needs to be determined

  #if the value is nil regex works object is not saved unless allow_nil is mentioned explicitly

  # validates :image_url, format: { with: /\.(jpg|gif)/, message: "is not in correct format" }

# => 6. validates_inclusion_of

  # validates :select_box, inclusion: { in: ["0","1","2"] }
  #object is never created if the given attribute is not in database

  #Also inclusion works as same way as exclusion

# => 7. validates_length_of

  #if the value is nil then it is not considered for length 0 even, object is not saved
  # validates_length_of :title, in: 0..9, message: "dguk"
  # in and within must be a range
  #if you specify message it replaces all three default messages

  # validates_length_of :title, is: 2
  #nonNegative Integer Or Infinity

  # validates_length_of :title, minimum: 2, maximum: 5
  #you can provide range using in or within or by using minimum and maximum

  # validates_length_of :permalink, is: 3, tokenizer: ->(str) { str.split('-') }

  # validates :permalink, length: {
  #   in: 5..6,
  #   tokenizer: ->(str) { str.split('-') },
  #   message: "booboobo0"
  # }

  # validates :permalink, length: {
  #   in: 6..8,
  #   minimum: 4,
  #   maximum: 7,
  #   is: 6,
  #   within: 1..9
  # }
  #In this case priority is given to in or within over minimum and maximum
  #first of in or within is considered as range given
  #first range is checked then accurate value is checked

# => 8. validates_numericality_of

  # validates_numericality_of :permalink, only_integer: true
  # validates_numericality_of :permalink, greater_than: 30.23, only_integer: true
  #if specified only_integer it check if it is a integer or not and shows only that error
  #further validations are skipped if it gives error

  # validates_numericality_of :permalink, other_than: :boo
  # validates_numericality_of :permalink, other_than: 23
  #error thrown is just its not a number

  # validates_numericality_of :permalink, odd: true
  # validates_numericality_of :permalink, even: true
  # even in only integer: false this validation removes decimal points just checks before of it
  #for odd and even without any rounding off

  # validates :permalink, numericality: {
  #   only_integer: true,
  #   greater_than: 12,
  #   less_than: 56,       #sequence
  #   other_than: 23,
  #   message: "boo"
  # }
  #same as length option, the message option will replace every kind of message

# => 9. validates_presence_of

  # validates_presence_of :title, :enabled, presence: true
  # it calls the blank? method on attribute
  # here anabled is a checkbox if not checked value goes as "0" which is mapped to false but false.blank? = true
  # even whitespaces are also considered blank

  # validates_presence_of :title, message: "fhg"

# => 10. validates_size_of

  # Alias for validates_length_of

  # validates :permalink, size: {
  #   in: 5..6,
  #   tokenizer: ->(str) { str.split('-') },
  #   message: "yoohoo"
  # }
  #this type in validates does not work

  # validates_size_of :permalink, is: 3, tokenizer: ->(str) { str.split('-') }
  #while this type works fine

# => 11. validates_uniqueness_of

  # validates_uniqueness_of :title, scope: :category
  #even if the category is nil there can only be one value of title for null category

  # validates_uniqueness_of :title, case_sensitive: false
  #affect the query by including binary

  # validates_uniqueness_of :title, conditions: -> { where(description: "") }
  #it only works for db methods like where not general if else works inside the block
  #self is the ActiveRecord::Relation object of the newly current created object

  # validates :title, uniqueness: {
  #   scope: [:category, :permalink],
  #   case_sensitive: false,
  #   conditions: -> { where(description: "hey there") }
  # }

  # validates_uniqueness_of :title, conditions: lambda { |post, pos| where(description: "") }
  # validates_uniqueness_of :title, conditions: ->(post) { where(description: "") }
  #shows some error needs to discuss

  validates :title, presence: true

  private


    def boo
      [23,24,25]
    end

end


