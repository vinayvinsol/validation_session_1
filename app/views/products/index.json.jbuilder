json.array!(@products) do |product|
  json.extract! product, :id, :title, :price, :discount_price, :enabled, :image_url, :category, :images, :permalink, :description
  json.url product_url(product, format: :json)
end
