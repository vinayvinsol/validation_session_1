json.extract! @product, :id, :title, :price, :discount_price, :enabled, :image_url, :category, :images, :permalink, :description, :created_at, :updated_at
